export default {
    "landing": "Landing page :)",
    "loading": "Loading...",
    "copyToClipBoard": "Click here to copy link",
    "copied": "Copied link!"
}