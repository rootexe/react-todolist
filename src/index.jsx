import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import reduxThunk from "redux-thunk";
import { createBrowserHistory } from "history";
import {
  connectRouter,
  routerMiddleware,
  ConnectedRouter
} from "connected-react-router";
import { ThemeProvider, createGlobalStyle } from "styled-components";
import { IntlProvider } from "react-intl";

import App from "./App";
import * as serviceWorker from "./serviceWorker";
import reducers from "./reducers";
import Async from "./middlewares/async";
import apiResult from "./middlewares/apiResult";
import themes from "./themes";
import colors from "./themes/color";
import localeData from "./locales";

import "./themes/global.css";

const messages = localeData.en;
const history = createBrowserHistory();
const store = createStore(
  connectRouter(history)(reducers(history)),
  {},
  compose(
    applyMiddleware(routerMiddleware(history), reduxThunk, Async, apiResult)
  )
);

const GlobalStyle = createGlobalStyle`
  :root {
    ${Object.keys(colors).map(
      hexColor => `--color-${hexColor}: ${colors[hexColor].getHex()};`
    )}
  }

  html,
  body,
  #root {
    font-family: 'Courier';
    color: ${props => props.theme.color3};
    background-color: ${props => props.theme.color3};
  }
`;

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <ThemeProvider theme={themes.base}>
        <>
          <GlobalStyle />
          <IntlProvider locale="en" messages={messages}>
            <App />
          </IntlProvider>
        </>
      </ThemeProvider>
    </ConnectedRouter>
  </Provider>,
  document.getElementById("root")
);

serviceWorker.register();
