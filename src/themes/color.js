class color {
  constructor(name, hex) {
    this.name = name;
    this.hex = hex;
  }

  getHex = () => this.hex;

  toString = () => `var(--color-${this.name})`;
}

const colors = {
  white: new color("white", "#ffffff"),
  black: new color("black", "#000000"),
  chestnutRose: new color("chestnutRose", "#cd5360"),
  alto: new color("alto", "#d4d4d4"),
  concrete: new color("concrete", "#f2f2f2"),
  mineShaft: new color("mineShaft", "#333333")
};

export default colors;
