import colors from "./color";

export default {
    color1: colors.white,
    color2: colors.black,
    color3: colors.mineShaft
};
