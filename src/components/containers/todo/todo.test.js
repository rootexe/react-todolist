const puppeteer = require("puppeteer");
jest.setTimeout(30000);

describe("we can launch a browser", () => {
  let browser, page, listUrl, pageRoute;

  beforeAll(async () => {
    browser = await puppeteer.launch({
      headless: false,
      timeout: 30000
    });

    page = await browser.newPage();
    await page.goto("https://yash-todo-app.herokuapp.com/");
  });

  describe("we should find and read the unique route", () => {
    it("should see textarea", async () => {
      urlRoute = await page.waitForSelector("#pageRoute");
      expect(urlRoute).not.toBe(null);
    });

    it("should read page route from textarea", async () => {
      pageRoute = await page.$("#pageRoute");
      const url = await (await pageRoute.getProperty("value")).jsonValue();
      expect(url).not.toBe(undefined);
      listUrl = url;
    });
  });

  describe("edit template to-do", () => {
    let firstTodo;

    it("should have added one todo", async () => {
      const todos = await page.$$(".todo-item");
      expect(todos.length).toEqual(1);
      firstTodo = todos[0];
    });

    it("should be 'Buy milk'", async () => {
      const textarea = await firstTodo.$("textarea");
      expect(await (await textarea.getProperty("value")).jsonValue()).toEqual(
        "Buy milk"
      );
    });

    it("should change to 'Buy bread'", async () => {
      const textarea = await firstTodo.$("textarea");
      await page.evaluate(el => (el.value = "Buy bread"), textarea);
      await page.evaluate(el => el.focus(), textarea);
    });

    it("should now say 'Buy bread'", async () => {
      const textarea = await firstTodo.$("textarea");
      expect(await (await textarea.getProperty("value")).jsonValue()).toEqual(
        "Buy bread"
      );
    });

    it("should change to done", async () => {
      const checkbox = await firstTodo.$("i");
      await checkbox.click();
    });

    it("should now be done", async () => {
      const checkbox = await firstTodo.$("i");
      expect(
        await (await checkbox.getProperty("className")).jsonValue()
      ).toContain("fa-check-square");
    });
  });

  describe("new to-do", () => {
    let lastTodo;

    it("should be able to click on new todo item", async () => {
      const newBtn = await page.$("i.fa-plus-circle");
      await newBtn.click();
    });

    it("should have a new todo", async () => {
      const todos = await page.$$(".todo-item");
      expect(todos.length).toEqual(2);
      lastTodo = todos[todos.length - 1];
    });

    it("should delete when empty", async () => {
      const textarea = await lastTodo.$("textarea");
      await page.evaluate(el => (el.value = ""), textarea);
      await page.evaluate(el => el.focus(), textarea);
      await page.evaluate(el => el.blur(), textarea);
    });

    it("should have been deleted", async () => {
      const todos = await page.$$(".todo-item");
      expect(todos.length).toEqual(1);
    });
  });

  describe("reload page with unique route", () => {
    beforeAll(async () => {
      await page.goto(listUrl);
      await page.waitForSelector("#pageRoute");
    });

    it("should now say 'Buy bread'", async () => {
      const todos = await page.$$(".todo-item");
      const textarea = await todos[0].$("textarea");
      expect(await (await textarea.getProperty("value")).jsonValue()).toEqual(
        "Buy bread"
      );
    });
  });

  afterAll(async () => {
    await browser.close();
  });
});
