import React, { PureComponent } from "react";
import styled from "styled-components";
import breakpoint from "styled-components-breakpoint";

import Stage from "./../../modules/stage";
import Loading from "./../../modules/loading";
import PageRoute from "./../../modules/pageRoute";
import ToDoItem from "./../../modules/todo";
import ToDoNew from "./../../modules/todo/newItem";

import { db } from "./../../../helpers/firebase";

const Header = styled.div`
  width: 100%;
  display: flex;
  overflow: hidden;
  height: 65px;
  justify-content: center;
  align-items: center;
  padding: 0px 25px;

  ${breakpoint("mobile")`
    padding: 0px 3vw;
  `}
  ${breakpoint("tablet")`
    padding: 0px 25px;
  `}
  ${breakpoint("desktop")`
    padding: 0px 25px;
  `}
`;

const Body = styled.div`
  flex: 1;
  overflow: hidden;
  overflow-y: auto;
  padding: 10px 25px;

  ${breakpoint("mobile")`
    padding: 1vw 3vw;
  `}
  ${breakpoint("tablet")`
    padding: 10px 25px;
  `}
  ${breakpoint("desktop")`
    padding: 10px 25px;
  `}
`;

const Title = styled.div`
  flex: 1;
  font-weight: 400;
  font-size: 32px;
  opacity: 0.4;

  ${breakpoint("mobile")`
    font-size: 18px;
  `}
  ${breakpoint("tablet")`
    font-size: 32px;
  `}
  ${breakpoint("desktop")`
    font-size: 32px;
  `}
`;

class Todo extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      listId: null,
      todos: null,
      loading: true
    };
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    if (id && this.state.listId !== id) {
      this.setState(
        {
          listId: id
        },
        this.loadToDo
      );
    } else if (!this.state.listId) {
      db.ref()
        .push({
          createdOn: new Date().toJSON()
        })
        .then(result => {
          const listId = result.key;
          db.ref(`/${result.key}/todo`)
            .push({
              text: "Buy milk",
              done: false,
              createdOn: new Date().toJSON()
            })
            .then(() =>
              this.setState(
                {
                  listId: listId
                },
                this.loadToDo
              )
            );
        });
    }
  }

  loadToDo = () => {
    if (!!this.state.todos) {
      return;
    }

    db.ref(`/${this.state.listId}/todo`).on("value", snapshot =>
      this.setState({
        todos: snapshot.val(),
        loading: false
      })
    );
  };

  handleUpdate = async (key, todo) =>
    await db.ref(`/${this.state.listId}/todo/${key}`).set(todo);

  handleOnNew = async () => {
    await db.ref(`/${this.state.listId}/todo`).push({
      text: "Buy milk",
      done: false,
      createdOn: new Date().toJSON()
    });
  };

  handleDelete = async key =>
    await db.ref(`/${this.state.listId}/todo/${key}`).set(null);

  render() {
    return (
      <Stage>
        {this.state.loading ? (
          <Loading />
        ) : (
          <>
            <Header>
              <Title>My todo list</Title>
              <PageRoute listId={this.state.listId} />
            </Header>
            <Body>
              {this.state.todos &&
                Object.keys(this.state.todos).map(key => {
                  const todo = this.state.todos[key];
                  return (
                    <ToDoItem
                      {...todo}
                      key={key}
                      id={key}
                      onUpdate={this.handleUpdate.bind(this, key)}
                      onDelete={this.handleDelete.bind(this, key)}
                    />
                  );
                })}
              <ToDoNew onClick={this.handleOnNew} />
            </Body>
          </>
        )}
      </Stage>
    );
  }
}

export default Todo;
