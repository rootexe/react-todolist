import React, { memo } from "react";
import styled from "styled-components";

const Body = styled.div`
  color: ${props => props.theme.color3};
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 12px;
  font-family: "ArialMT", "Arial";
`;

const Container = styled.button`
  height: 24px;
  border: 0;
  cursor: pointer;
  position: relative;
  background-color: transparent;
  padding: 0 10px;
  border: ${props => props.outline ? "1" : "0"}px solid ${props => props.theme.color3};
  outline: 0;
  border-radius: 4px;
  overflow: hidden;

  i {
    margin-right: 5px;
  }

  ${Body} {
    opacity: ${props => props.outline ? "1" : ".8"};
  }

  &:before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: ${props => props.theme.color3};
    opacity: ${props => props.outline ? "0" : ".1"};
    z-index: -1;
  }

  &:hover,
  &:focus {
    outline: 0;
  }
`;

export default memo(({ children, ...props }) => {
  return (
    <Container {...props}>
      <Body>{children}</Body>
    </Container>
  );
});
