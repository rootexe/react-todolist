import React, { memo } from "react";
import styled from "styled-components";

const Container = styled.div`
  font-size: 14px;
  font-weight: 400;
  color: ${props => props.theme.color2};
  display: flex;
  opacity: .2;
  cursor: pointer;

  &:hover {
    opacity: 1;
  }
`;

const Icon = styled.i`
  font-size: 16px;
  text-align: center;
  margin-right: 15px;
  color: ${props => props.theme.color2};
`;

export default memo(({ onClick }) => (
  <Container onClick={onClick}>
    <Icon className="fas fa-plus-circle" />
    <div>Add new line</div>
  </Container>
));
