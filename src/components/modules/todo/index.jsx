import React, { PureComponent } from "react";
import styled from "styled-components";
import TextareaAutosize from 'react-autosize-textarea';

const Container = styled.div`
  width: 100%;
  display: flex;
`;

const Checkbox = styled.div`
  margin-right: 5px;
  display: flex;
  align-items: center;
  max-height: 16px;
  color: ${props => props.theme.color3};

  i {
    margin-top: 5px;
    cursor: ${props => (props.placeHolder ? "default" : "pointer")};
  }
`;

const Input = styled(TextareaAutosize)``;

const InputContainer = styled.div`
  flex: 1;

  ${Input} {
    width: 100%;
    font-size: 14px;
    color: ${props => props.theme.color2};
    text-decoration: ${props => (props.done ? "line-through" : "none")};
    font-weight: 400;
    resize: none;
    overflow: hidden;
    border: none;
    background-color: transparent;
    margin-bottom: 15px;
    padding-left: 10px;
    opacity: ${props => (props.done ? ".5" : "1")};

    &:hover,
    &:focus {
      outline: none;
    }

    &:focus {
      color: ${props => props.theme.color2};
    }
  }
`;

class ToDo extends PureComponent {
  constructor(props) {
    super(props);
    this.input = React.createRef();
  }

  handleOnBlur = event => {
    const elm = event.target;
    if (elm.value) {
      return this.handleUpdate(elm.value, null);
    }

    this.props.onDelete();
  };

  toggleDone = () => {
    this.handleUpdate(null, !this.props.done);
  };

  handleUpdate = (text, done) => {
    this.props.onUpdate({
      text: text != null ? text : this.props.text,
      done: done != null ? done : this.props.done
    });
  };

  render() {
    const { done, text, id } = this.props;

    return (
      <Container className="todo-item" id={id}>
        <Checkbox done={done}>
          <i
            className={`fa${done ? "s" : "l"} fa-${done ? "check-square" : "stop"}`}
            onClick={this.toggleDone}
          />
        </Checkbox>
        <InputContainer done={done} text={text}>
          <Input
            disabled={!done ? false : true}
            onBlur={this.handleOnBlur}
            defaultValue={text || "Buy milk"}
            ref={this.input}
          />
        </InputContainer>
      </Container>
    );
  }
}

export default ToDo;
