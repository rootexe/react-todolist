import styled from "styled-components";
import breakpoint from "styled-components-breakpoint";

const style = {
  width: {
    desktop: "85%",
    tablet: "95%",
    mobile: "95%"
  },
  height: {
    desktop: "85%",
    tablet: "95%",
    mobile: "95%"
  },
  borderRadius: "10px"
};

export default styled.div`
  background-color: ${props => props.theme.color1};
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
  border-radius: ${style.borderRadius};
  overflow: hidden;
  max-height: 100%;
  display: flex;
  flex-direction: column;
  overflow: hidden;
  box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);

  ${breakpoint("mobile")`
    width: ${style.width.mobile};
    height: ${style.height.mobile};
  `}  
  ${breakpoint("tablet")`
    width: ${style.width.tablet};
    height: ${style.height.tablet};
  `}
  ${breakpoint("desktop")`
    width: ${style.width.desktop};
    height: ${style.height.desktop};
  `}
`;
