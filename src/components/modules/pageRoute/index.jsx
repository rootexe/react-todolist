import React, { PureComponent } from "react";
import { FormattedMessage } from "react-intl";
import styled from "styled-components";

import Button from "./../../modules/button";

const Textarea = styled.textarea`
  position: fixed;
  top: 0;
  left: 0;
  height: 0px;
  width: 0px;
  z-index: -1;
  border: 0;
  resize: none;
  opacity: 0;
`;

class Header extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      copied: false
    };
    this.url = React.createRef();
  }

  copyToClipboard = () => {
    const el = this.url.current;
    el.select();
    document.execCommand("copy");
    if (window.getSelection) {
      window.getSelection().removeAllRanges();
    } else if (document.selection) {
      document.selection.empty();
    }
    this.setState({
      copied: true
    });
  };

  handleMouseLeave = () => {
    setTimeout(() => {
      if (!this.state.copied) {
        return;
      }

      this.setState({
        copied: false
      });
    }, 500);
  };

  render() {
    const url = `${window.location.protocol}//${window.location.host}/${this
      .props.listId || ""}`;
    return (
      <>
        <Textarea ref={this.url} value={url} readOnly={true} id="pageRoute" />
        <FormattedMessage id={this.state.copied ? "copied" : "copyToClipBoard"}>
          {local => (
            <Button outline={this.state.copied} onClick={this.copyToClipboard}>
              <i class="far fa-link" />
              <div>{local}</div>
            </Button>
          )}
        </FormattedMessage>
      </>
    );
  }
}

export default Header;
