import React, { memo } from "react";
import { Spinner } from "@blueprintjs/core";
import styled from "styled-components";

const Container = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
`;

export default memo(() => (
  <Container>
    <Spinner />
  </Container>
));
