import React, { PureComponent } from "react";
import { Route } from "react-router-dom";
import { connect } from "react-redux";

import Loading from "./components/containers/loading";
import ToDo from "./components/containers/todo";
import actions from "./actions";
import firebase from "./helpers/firebase";

class App extends PureComponent {
  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      this.props.user_set(user);
      this.props.appState_set_updating(false);
    });
  }

  render() {
    if (this.props.appState.loading) {
      return <Route component={Loading} />;
    }

    return <Route path="/:id?" component={ToDo} />;
  }
}

function mapStateToProps({ appState, user }) {
  return {
    appState,
    user
  };
}

const mapDispatchToProps = {
  ...actions.appState,
  ...actions.user
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
