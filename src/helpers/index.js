import axios from "axios";

export const request = {
  post: (path, payload) => apiRequest(axios.post, path, payload),
  get: path => apiRequest(axios.get, path),
  put: (path, payload) => apiRequest(axios.put, path, payload),
  patch: async (path, payload) => await apiRequest(axios.patch, path, payload),
  delete: path => apiRequest(axios.delete, path)
};

const apiRequest = (request, path, payload) => {
  return new Promise((resolve, reject) => {
    let req = null;

    if (payload) {
      req = request(path, payload);
    } else {
      req = request(path);
    }

    req
      .then(res => resolve(res.data), err => reject(err.response))
      .catch(err => reject(err));
  });
};
