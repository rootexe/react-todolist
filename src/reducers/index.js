import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import appStateReducer from "./appStateReducer";
import userReducer from "./userReducer";

export default history =>
  combineReducers({
    router: connectRouter(history),
    appState: appStateReducer,
    user: userReducer
  });
