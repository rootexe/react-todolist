import types from "./../actions/user/types";

const defaultValue = null;

export default function(state = defaultValue, action) {
  switch (action.type) {
    case types.USER_SET:
      return action.payload;
    default:
      return state;
  }
}
