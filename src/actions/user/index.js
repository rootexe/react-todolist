import types from "./types";

export const user_set = user => async dispatch => {
  dispatch({ type: types.USER_SET, payload: user });
};

export default {
    user_set
};
